import Vue from "vue";
import VueNativeSock from "vue-native-websocket";
import App from "./App";
import router from "./router";

Vue.use(VueNativeSock, "ws://echo.websocket.org");

Vue.config.productionTip = false;

new Vue({
  el: "#app",
  router,
  components: { App },
  template: "<App/>"
});
