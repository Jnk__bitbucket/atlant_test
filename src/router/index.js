import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Calc",
      component: () => import("@/components/Calc")
    },
    {
      path: "/comments",
      name: "Comments",
      component: () => import("@/components/Comments")
    }
  ]
});
