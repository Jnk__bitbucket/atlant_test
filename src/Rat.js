const gcd = (a, b) => (b === 0 ? a : gcd(b, a % b));

export default class Rat {
  static of(numer, denom) {
    return new Rat(numer, denom);
  }

  get numer() {
    return this._numer;
  }

  get denom() {
    return this._demon;
  }

  constructor(numer, denom) {
    const g = gcd(numer, denom);
    const abbr_numer = numer / g;
    const abbr_denom = denom / g;
    this._numer = isNaN(abbr_numer) ? numer : abbr_numer;
    this._demon = isNaN(abbr_denom) ? denom : abbr_denom;
  }
}

export const add = (x, y) =>
  Rat.of(x.numer * y.denom + y.numer * x.denom, x.denom * y.denom);
export const sub = (x, y) =>
  Rat.of(x.numer * y.denom - y.numer * x.denom, x.denom * y.denom);
export const mul = (x, y) => Rat.of(x.numer * y.numer, x.denom * y.denom);
export const div = (x, y) => Rat.of(x.numer * y.denom, x.denom * y.numer);
